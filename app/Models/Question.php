<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function survey()
    {
        return $this->belongsTo('Encuestas\Models\Survey', 'survey_id', 'id');
    }

    public function question_type()
    {
        return $this->belongsTo('Encuestas\Models\QuestionType', 'question_type_id', 'id');
    }

    public function answer_alternatives()
    {
        return $this->hasMany('Encuestas\Models\AnswerAlternative', 'question_id', 'id');
    }

    public function answers()
    {
        return $this->hasManyThrough('Encuestas\Models\Answer','Encuestas\Models\AnswerAlternative', 'question_id', 'answer_alternative_id');
    }

}
