<?php

namespace Encuestas\Http\Controllers\API;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\ApiController;
use Encuestas\Libraries\Response;

use Auth;
use Log;

use Encuestas\Models\Answer;
use Encuestas\Models\Survey;
use Encuestas\Models\User;
use Encuestas\Models\UserSurvey;

class SurveyController extends ApiController
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api-auth');
    }

    public function getIndex()
    {
        $user = Auth::user();

        $completed_surveys_ids = UserSurvey::where('user_id', $user->id)
            ->get()
            ->lists('survey_id')
            ->toArray();

        $single_surveys = Survey::whereNotIn('id', $completed_surveys_ids)
        ->where('survey_state_id', 2)
        ->has('survey_constraints', '=', 0)
        ->get()->lists('id')->toArray();

        $constraint_surveys = Survey::whereNotIn('id', $completed_surveys_ids)
        ->where('survey_state_id', 2)
        ->has('survey_constraints')->get();

        $approved_constrainted_surveys = array();

        foreach($constraint_surveys as $_survey) {
            if($_survey->has_aproved_constraints($user)) {
                array_push($approved_constrainted_surveys, $_survey->id);
            }
        }

        $approved_surveys = array_merge($single_surveys, $approved_constrainted_surveys);

        $surveys = Survey::whereIn('id', $approved_surveys)
            ->with(
                'questions.question_type',
                'questions.answer_alternatives.alternative_type'
            )
            ->get()
            ->toArray();

        return Response::ok($surveys);
    }

    public function postStore(Request $request)
    {
        try {
            
            $user = auth()->user();

            $data = json_decode( $request->get('data') );

            $survey_id = $data->survey_id;

            $survey = Survey::findOrFail($survey_id);

            $usersurvey_exists = UserSurvey::where('user_id', $user->id)->where('survey_id', $survey->id)->get();
            
            if( !$usersurvey_exists->isEmpty() )
            {
                return Response::ok(['message' => 'Ud. ya realizo esta encuesta en el pasado.']);
            }

            if($survey->max_completed_per_survey <= $survey->current_completed_surveys) {
                return Response::ok(['reward_points_earned' => 0, 'total_reward_points' => $user->reward_points, 'message' => 'Ud. no ha ganado puntos por esta encuesta.']);
            }

            foreach ($data->answers as $answer_data) {

                $answer_alternative_id = $answer_data->answer_alternative_id;

                $answer = new Answer;
                $answer->answer_alternative_id = $answer_alternative_id;

                $answer->save();

            }

            $usersurvey = new UserSurvey;
            $usersurvey->user_id = $user->id;
            $usersurvey->survey_id = $survey->id;

            $usersurvey->save();

            $survey->current_completed_surveys++;
            if($survey->max_completed_per_survey == $survey->current_completed_surveys) {
                $survey->survey_state_id = 3;
            }
            $survey->save();

            $user->reward_points += $survey->reward_points;
            $user->save();

            return Response::ok(['reward_points_earned' => $survey->reward_points, 'total_reward_points' => $user->reward_points, 'message' => 'Ud. ha ganado '.$survey->reward_points.' puntos. Tiene un total de '.$user->reward_points.' puntos.']);
            
        } catch (Exception $e) {
            return Response::internalServerError();
        }
    }
}
