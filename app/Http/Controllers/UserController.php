<?php

namespace Encuestas\Http\Controllers;

use Validator;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\Controller;

use Encuestas\Models\User;
use Encuestas\Libraries\Response;

use PaulVL\Helpers\StringHelper;

class UserController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*Uncomment on production

        */
    }

    public function getIndex()
    {
        return redirect()->route('dashboard');
    }

    public function postRegistro(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make( $inputs, User::$rules_usuario );
        
        if ($validator->fails()) {
            $validation_errors = StringHelper::concatInOneLine( $validator->errors()->all(), ' ' );
            return Response::unprocessableEntity( $validation_errors );
        }

        try {
            $user = new User;

            $user->user_type_id = 3;
            $user->id_doc_number = $inputs['id_doc_number'];
            $user->first_name = $inputs['first_name'];
            $user->last_name = $inputs['last_name'];
            $user->phone = StringHelper::empty_to_null( $inputs['phone'] );
            $user->birth_date = $inputs['birth_date'];
            $user->gender = $inputs['gender'];
            $user->email = $inputs['email'];
            $user->password = bcrypt( $inputs['password'] );

            $user->save();

            return Response::created();
        } catch (Exception $e) {
            return Response::internalServerError();
        }
    }

    public function getMiCuenta()
    {
        $this->pagename = 'Mi Cuenta';
        $this->pagetitle = 'Mi Cuenta';
        $this->subtitle = '';

        return $this->view('app.users.profile');
    }

    public function postMiCuenta(Request $request)
    {
        $new_password = trim( $request->get('password') );
        if(StringHelper::is_empty($new_password)) {
            return redirect()->back();
        }else{

            $validator = Validator::make( ['password' => $new_password], ['password' => 'required|min:6'] );
            
            if ($validator->fails()) {
                $validation_errors = $validator->errors()->all();
                return redirect()->back()->withValidationErrors($validation_errors);
            }

            auth()->user()->password = bcrypt( $new_password );
            auth()->user()->save();
            
            return redirect()->back()->withSuccess('Contraseña actualizada correctamente.');
        }
    }

    public function getConfiguracion(Request $request)
    {
        $this->pagename = 'Configuración';
        $this->pagetitle = 'Configuración';
        $this->subtitle = '';

        return $this->view('app.users.configuration');
    }

    public function postConfiguracion(Request $request)
    {
    }
}
