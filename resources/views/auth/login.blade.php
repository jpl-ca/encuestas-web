@extends('app.layouts.master')

@section('master-container')

    <div class="container">
        <div class="row">
            @include('app.layouts.fragments.alerts')
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img src="{{ asset('images/encuestas-logo-200.png')}}" class="img img-responsive center-block" alt="">
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" action="{{ url('auth/login') }}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group {{ !empty(old('email')) ? 'has-error' : '' }}">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="contraseña" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="remember">No cerrar sesión
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
