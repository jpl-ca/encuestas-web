@extends('app.layouts.base')

@section('base-container')
	<div class="col-sm-12">
		<form class="form-horizontal" action="{{ route('app.users.update-profile') }}" method="POST">
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-sm-6">

		        	<h2 class="section-header">Notificaciones</h2>
		        	
					<div class="form-group">
						<label class="col-sm-10 control-label">Notificarme cuando una encuesta se active</label>
						<div class="col-sm-2">
							<input type="checkbox" name="notify_when_activated" checked>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-10 control-label">Notificarme cuando una encuesta expire o se complete</label>
						<div class="col-sm-2">
							<input type="checkbox" name="notify_when_expired_completed" checked>
						</div>
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<button type="submit" class="btn btn-info btn-lg center-block">Guardar</button>
				</div>
			</div>
		</form>
	</div>

@stop

@section('js')
	<script>
		$( document ).ready(function() {
			$("[name='notify_when_activated']").bootstrapSwitch();
			$("[name='notify_when_expired_completed']").bootstrapSwitch();
		});
	</script>
@stop