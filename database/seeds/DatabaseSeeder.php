<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        Model::unguard();
        /*
        factory('Encuestas\Models\User', 'usuario', 40)->create()->each(function($u) use($faker) {
            //$u->ticket_replies()->saveMany(factory('Encuestas\Models\TicketReply', 'agent_reply', 3)->make());
        });
        */
        factory(Encuestas\Models\User::class, 'admin', 2)->create();

        factory(Encuestas\Models\User::class, 'suscriptor', 10)->create()->each( function($suscriptor) use($faker) {

            $suscriptor->subscription_history()->save(new Encuestas\Models\SubscriptionHistory([
                    'name' => 'Vitalicio',
                    'price' => '0.00',
                    'start_date' => \Carbon\Carbon::now(),
                    'end_date' => \Carbon\Carbon::now()->addDays(21900),
                    'available_surveys' => -1,
                    'max_completed_per_survey' => -1,
                    'available_completed_surveys' => -1
                ]));

            $suscriptor->surveys()->saveMany(factory(Encuestas\Models\Survey::class, 2)->make())->each( function($survey) use ($faker) {
                $survey->questions()->saveMany(factory(Encuestas\Models\Question::class, 5)->make())->each( function($question) use ($faker) {

                    $question_type_id = $question->question_type_id;

                    if($question_type_id == 1){
                        $has_free_answer = $faker->boolean($chanceOfGettingTrue = 30);
                        if($has_free_answer){
                            $question->answer_alternatives()->saveMany(factory(Encuestas\Models\AnswerAlternative::class, 'unica', 4)->make());
                            $question->answer_alternatives()->save(factory(Encuestas\Models\AnswerAlternative::class, 'libre')->make());
                        }else{
                            $question->answer_alternatives()->saveMany(factory(Encuestas\Models\AnswerAlternative::class, 'unica', 5)->make());
                        }
                    }elseif($question_type_id == 2){
                        $question->answer_alternatives()->saveMany(factory(Encuestas\Models\AnswerAlternative::class, 'multiple', 5)->make());
                    }else{
                        $question->answer_alternatives()->save(factory(Encuestas\Models\AnswerAlternative::class, 'rango')->make());
                    }
                });
            });           
        });

        factory(Encuestas\Models\User::class, 'usuario', 100)->create();

        Model::reguard();
    }
}
