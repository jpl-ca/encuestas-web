<?php

namespace Encuestas\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use Encuestas\Models\Survey;
use Encuestas\Models\User;

use \Encuestas\Libraries\Pusher;

class FinishSurveys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:finish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finish active surveys.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->finishSurveys();
    }

    private function finishSurveys() {
        
        $start_of_day = Carbon::now()->startOfDay()->toDateTimestring();

        $survey_to_be_updated = Survey::where('survey_state_id', 2)
            ->where('end_date', '<', $start_of_day)
            ->get()->lists('id')->toArray();

        $updated_count = Survey::whereIn('id', $survey_to_be_updated)
            ->update(['survey_state_id' => 3]);
        
        \Log::info( 'TAREA: Finalizando Encuestas. Ejecutada a las '.Carbon::now()->toDateTimestring().' total de encuestas finalizadas: '. $updated_count);
        
    }
}



