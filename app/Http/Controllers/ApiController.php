<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;
use Encuestas\Libraries\Response;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use PaulVL\Helpers\StringHelper;

abstract class ApiController extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function rawStore(Request $request, array $rules, $model_class)
    {
        $inputs = $request->all();

        $validator = Validator::make( $inputs, $rules );
        
        if ($validator->fails()) {
            $validation_errors = StringHelper::concatInOneLine( $validator->errors()->all(), ' ' );
            return Response::unprocessableEntity( $validation_errors );
        }

        try {
            $object = $model_class::create( $inputs );
            return Response::created();
        } catch (Exception $e) {
            return Response::internalServerError();
        }
    }
}
