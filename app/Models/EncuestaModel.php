<?php

namespace Encuestas\Models;

use Illuminate\Database\Eloquent\Model;

class EncuestaModel extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function asDate($attr, $format = 'Y-m-d') {
    	if(is_null($this->$attr)) return '';
    	return (new \Carbon\Carbon($this->$attr))->format($format);
    }
    
}
