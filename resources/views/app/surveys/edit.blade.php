@extends('app.layouts.base')

@section('base-container')

    <div class="row">
        <div class="col-sm-12 margin-bottom-20">
        	<a href="{{ route('app.surveys.index') }}" class="btn btn-danger pull-right">Cancelar</a>
    	</div>
    </div>

    <div class="row">
        <div class="col-sm-12">

			<div class="panel panel-default">
                <div class="panel-heading">
                    Información básica
                </div>
				
				<form role="form" action="{{ route('app.surveys.update', $survey->id) }}" method="POST" id="newsurveyform">
					{!! csrf_field() !!}
	                <div class="panel-body">
	                    <div class="form-group form-group-lg">
	                        <input class="form-control" name="survey_title" id="survey_title" value="{{ $survey->title }}" placeholder="Título de la encuesta" required>
	                    </div>

						<div class="row margin-bottom-35">
							<div class="col-sm-6">

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="start_date" class="col-sm-5 control-label">Fecha de inicio</label>
											<div class="col-sm-7">
									            <div class='input-group date' id='start_date'>
									                <input type='text' class="form-control" name="start_date" value="{{ $survey->asDate('start_date') }}" required/>
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
											</div>
										</div>

									</div>
								</div>

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="end_date" class="col-sm-5 control-label">Fecha de fin</label>
											<div class="col-sm-7">
									            <div class='input-group date' id='end_date'>
									                <input type='text' class="form-control" name="end_date" value="{{ $survey->asDate('end_date') }}"/>
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
											</div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-5 control-label">Cantidad de encuestados</label>
											<div class="col-sm-7">
									            <input type='text' class="form-control" name="max_completed_per_survey" value="{{ $survey->max_completed_per_survey }}" max="{{ $max_available_surveys }}" required/>
									        </div>
										</div>

									</div>
								</div>

							</div>
							<div class="col-sm-6">

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-4 control-label">Genero</label>
											{{--*/
												$has_gender_constraint = $survey->has_gender_constraint();
												$has_age_constraint = $survey->has_age_constraint();
											/*--}}
											<div class="col-sm-8">
									            <select class="form-control" name="gender_constraint">
													@if($has_gender_constraint)
													{{--*/
														$gender_constraint = $survey->gender_constraint();
													/*--}}
													<option value="-1">Ambos</option>
														@if($gender_constraint->value1 == 0)
														<option value="0" selected>Femenino</option>
														<option value="1">Masculino</option>
														@else
														<option value="0">Femenino</option>
														<option value="1" selected>Masculino</option>
														@endif
													@else
													<option value="-1">Ambos</option>
													<option value="0">Femenino</option>
													<option value="1">Masculino</option>
													@endif
												</select>
									        </div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-2 control-label">Edad</label>
											<div class="col-sm-10">
												<div class="checkbox">
													<label>
														@if($has_age_constraint)
														<input type="checkbox" name="enable_age_constraints" id="enable_age_constraints" value="1" aria-label="..." checked>Activar
														@else
														<input type="checkbox" name="enable_age_constraints" id="enable_age_constraints" value="1" aria-label="...">Activar
														@endif
													</label>
												</div>

											</div>
										</div>

									</div>
								</div>												
								
								@if($has_age_constraint)
								{{--*/
									$age_constraint = $survey->age_constraint();
								/*--}}
								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<div class="col-sm-2">
												<div class="radio">
													<label>
														@if(is_null($age_constraint->value3))
														<input type="radio" name="age_constraint_type" id="age_constraint_type" value="1" aria-label="..." checked>
														@else
														<input type="radio" name="age_constraint_type" id="age_constraint_type" value="1" aria-label="..." >
														@endif
													</label>
												</div>
									        </div>
											<div class="col-sm-5">
									            <select class="form-control" name="age_comparison" id="age_comparison">
													@if($age_constraint->value2 == '=')
													<option value="=" selected>iguales a</option>
													<option value=">">mayores a</option>
													<option value="<">menores a</option>
													@elseif($age_constraint->value2 == '>')
													<option value="=">iguales a</option>
													<option value=">" selected>mayores a</option>
													<option value="<">menores a</option>
													@elseif($age_constraint->value2 == '<')
													<option value="=">iguales a</option>
													<option value=">">mayores a</option>
													<option value="<" selected>menores a</option>
													@else
													<option value="=">iguales a</option>
													<option value=">">mayores a</option>
													<option value="<">menores a</option>
													@endif
												</select>
									        </div>
											<div class="col-sm-5">
												@if(is_null($age_constraint->value3))
									            <input type='text' class="form-control" name="age_comparison_value" id="age_comparison_value" value="{{ $age_constraint->value1 }}" max="100" required/>
												@else
									            <input type='text' class="form-control" name="age_comparison_value" id="age_comparison_value" value="18" max="100" required/>
									            @endif
									        </div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">


											<div class="row">

												<div class="col-sm-2">
													<div class="col-sm-2">
														<div class="radio">
															<label>
															@if(!is_null($age_constraint->value3))
																<input type="radio" name="age_constraint_type" id="age_constraint_type" value="2" aria-label="..." checked>
															@else
																<input type="radio" name="age_constraint_type" id="age_constraint_type" value="2" aria-label="...">
															@endif
															</label>
														</div>
													</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">desde</label>
														<div class="col-sm-8">
															@if(!is_null($age_constraint->value3))
												            <input type='text' class="form-control" name="age_min" id="age_min" value="{{ $age_constraint->value1}}" max="99" required/>
															@else
												            <input type='text' class="form-control" name="age_min" id="age_min" value="18" max="99" required/>
												            @endif
												        </div>

									        		</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">hasta</label>
														<div class="col-sm-8">
															@if(!is_null($age_constraint->value3))
												            <input type='text' class="form-control" name="age_max" id="age_max" value="{{$age_constraint->value4}}" max="100" required/>
															@else
												            <input type='text' class="form-control" name="age_max" id="age_max" value="25" max="100" required/>
												            @endif
												        </div>

									        		</div>
									        	</div>

									        </div>

										</div>

									</div>
								</div>

								@else
								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<div class="col-sm-2">
												<div class="radio">
													<label>
														<input type="radio" name="age_constraint_type" id="age_constraint_type" value="1" aria-label="..." checked>
													</label>
												</div>
									        </div>
											<div class="col-sm-5">
									            <select class="form-control" name="age_comparison" id="age_comparison">
													<option value="=">iguales a</option>
													<option value=">">mayores a</option>
													<option value="<">menores a</option>
												</select>
									        </div>
											<div class="col-sm-5">
									            <input type='text' class="form-control" name="age_comparison_value" id="age_comparison_value" value="18" max="100" required/>
									        </div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">


											<div class="row">

												<div class="col-sm-2">
													<div class="col-sm-2">
														<div class="radio">
															<label>
																<input type="radio" name="age_constraint_type" id="age_constraint_type" value="2" aria-label="...">
															</label>
														</div>
													</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">desde</label>
														<div class="col-sm-8">
												            <input type='text' class="form-control" name="age_min" id="age_min" value="18" max="99" required/>
												        </div>

									        		</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">hasta</label>
														<div class="col-sm-8">
												            <input type='text' class="form-control" name="age_max" id="age_max" value="25" max="100" required/>
												        </div>

									        		</div>
									        	</div>

									        </div>

										</div>

									</div>
								</div>
								@endif

							</div>
						</div>

	                </div>

	                <div class="panel-footer">
							
						<button type="submit" class="btn btn-info pull" id="submit">Guardar</button>
	                </div>

	            </form>
            </div>

        </div>
    </div>

@stop

@section('js')
    <script src="{{ asset('js/edit-survey.js')}}"></script>
@stop