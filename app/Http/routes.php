<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('test', function (Illuminate\Http\Request $request) {
	//echo phpinfo();
	//exit();
	//return dd(\DB::connection());

	//$command = sprintf('%s mysqldump --user=%s --password=%s --host=%s %s > %s',
	//		"D:\backups",
			/*escapeshellarg("ekuestion"),
			escapeshellarg("ekuestion"),
			escapeshellarg("localhost"),
			//escapeshellarg($this->port),
			escapeshellarg("ekuestion"),
			escapeshellarg("bk.sql")*/
	//	);
	//$command = "D:\wamp\bin\mysql\mysql5.6.17\bin\mysqldump.exe -e -f -u ekuestion -pekuestion ekuestion > D:\bb.sql";
	//return dd($command);
	//\Artisan::call($command);
	//exec($command);
	//return 'correcto';
	//	return $this->console->run($command);


	// API access key from Google API's Console
	//$gcm_id = 'APA91bF_ITYGaESuTTVUJiqWVP1NC6oT-kx9TZkKyiUS0WsAOeQGtt39nb0B1i-wguMupbGADhl39KZDU2IbzTJzVQtE-jwCiaWz5LK6AXKO2t12W0IpdFo6XcrVJXoHJ2KivJYHHjfv';
	//
	//
	$v = '';
	if($v) {
		return 'si';
	}
   	return 'no';

});


Route::get('image', function (Illuminate\Http\Request $request) {
	return view('image');
});

Route::post('image', function (Illuminate\Http\Request $request) {
	$msg = '';

	if($request->has('image_url') || $request->hasFile('image_file')){
		$msg .= 'Si hay imagen, ';
	}

	return $msg;

	if($request->hasFile('image_file')){
		$image = $request->file('image_file');
		\Encuestas\Libraries\Imager::save($image);
		$msg = 'imagen subida';
	}else{
		$msg = 'no hay imagen';
	}
	return $msg;
});


Route::group( [ 'namespace' => 'API', 'prefix' => 'api' ], function ()
{

	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);

	Route::controller('survey', 'SurveyController', [
		'getIndex' => 'api.survey.index',
		'postStore' => 'api.survey.store'
	]);

	Route::controller('user', 'UserController', [
		'postRegister' => 'api.user.register',
		'postUpdateLastPosition' => 'api.user.update-last-position',
	]);
	
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::controller('mis-encuestas', 'SurveyController', [
    'getIndex' => 'app.surveys.index',
    'getCrear' => 'app.surveys.create',
    'postGuardar' => 'app.surveys.store',
    'getVerResultados' => 'app.surveys.results',
    'getFinalizar' => 'app.surveys.finish',
    'getActivar' => 'app.surveys.activate',
    'getDesactivar' => 'app.surveys.deactivate',
    'getEliminar' => 'app.surveys.delete',
    'getEditar' => 'app.surveys.edit',
    'postActualizar' => 'app.surveys.update',
]);

Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'AppController@getDashboard']);

Route::get('manual-de-usuario', function(Illuminate\Http\Request $request){

    $pagename = 'Manual de usuario';
    $pagetitle = 'Manual de usuario';
    $subtitle = '';

    $manual = Encuestas\Libraries\UserManual::getManual();

	return view('app.user-manual.index', compact('pagename','pagetitle','subtitle','manual'));
});

Route::controller('/', 'UserController', [
    'getMiCuenta' => 'app.users.profile',
    'postMiCuenta' => 'app.users.update-profile',
    'getConfiguracion' => 'app.users.configuration',
    'postConfiguracion' => 'app.users.update-configuration',
]);