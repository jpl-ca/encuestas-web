<?php

namespace Encuestas\Http\Controllers\API;

use Validator;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\ApiController;
use Auth;

use Encuestas\Models\User;
use Encuestas\Libraries\Response;

use PaulVL\Helpers\StringHelper;

class UserController extends ApiController
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api-auth', ['except' => ['postRegister']]);
        /*Uncomment on production

        */
    }

    public function postRegister(Request $request)
    {
        $inputs = $request->all();

        $validator = Validator::make( $inputs, User::$rules_usuario );
        
        if ($validator->fails()) {
            $validation_errors = StringHelper::concatInOneLine( $validator->errors()->all(), ' ' );
            return Response::unprocessableEntity( $validation_errors );
        }

        try {
            $user = new User;

            $user->user_type_id = 3;
            $user->id_doc_number = $inputs['id_doc_number'];
            $user->first_name = $inputs['first_name'];
            $user->last_name = $inputs['last_name'];
            $user->phone = StringHelper::empty_to_null( $inputs['phone'] );
            $user->birth_date = $inputs['birth_date'];
            $user->gender = $inputs['gender'];
            $user->email = $inputs['email'];
            $user->password = bcrypt( $inputs['password'] );

            $user->save();

            if ( Auth::attempt( ['id_doc_number' => $inputs['id_doc_number'], 'password' => $inputs['password'], 'user_type_id' => 3], true ) ) {
                return Response::ok(Auth::user()->toArray());
            }

            return Response::ok();

        } catch (Exception $e) {
            return Response::internalServerError();
        }
    }

    public function postUpdateLastPosition(Request $request)
    {
        auth()->user()->last_position = $request->get('last_position');
        auth()->user()->save();
        return Response::ok();
    }

    public function postUpdateGcmId(Request $request)
    {
        $kk = array_keys($request->all());
        $dd = implode(",", $kk);
        \Log::info('Se esta llamando al metodo actualizar gcmId con la data: '.$dd);

        auth()->user()->gcm_id = $request->get('gcm_id');
        auth()->user()->save();
        return Response::ok();
    }

    public function postUpdate(Request $request, $id)
    {
        //
    }
}
