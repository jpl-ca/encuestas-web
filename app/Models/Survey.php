<?php

namespace Encuestas\Models;

class Survey extends EncuestaModel
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public function survey_state()
    {
        return $this->belongsTo('Encuestas\Models\SurveyState', 'survey_state_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('Encuestas\Models\User', 'belongs_to_user_id', 'id');
    }

    public function allowed_users()
    {
        return $this->hasManyThrough('Encuestas\Models\User','Encuestas\Models\UserSurvey', 'survey_id', 'user_id');
    }

    public function questions()
    {
        return $this->hasMany('Encuestas\Models\Question', 'survey_id', 'id');
    }

    public function survey_constraints()
    {
        return $this->hasMany('Encuestas\Models\SurveyConstraint', 'survey_id', 'id');
    }

    public function has_constraints()
    {
        if(count($this->survey_constraints()->get()->toArray()) < 1) {
            return false;
        }
        return true;
    }

    public function gender_constraint()
    {
        return $this->survey_constraints()->where('survey_constraint_type_id', 2)->first();
    }

    public function has_gender_constraint()
    {
        if(is_null($this->gender_constraint())) {
            return false;
        }
        return true;
    }

    public function age_constraint()
    {
        return $this->survey_constraints()->where('survey_constraint_type_id', 1)->first();
    }

    public function has_age_constraint()
    {
        if(is_null($this->age_constraint())) {
            return false;
        }
        return true;
    }

    public function has_aproved_constraints(User $user)
    {        
        $approved_constraints = 0;
        $survey_constraints = $this->survey_constraints;

        foreach ($survey_constraints as $constraint) {
            if($constraint->survey_constraint_type_id == 2){
                if($constraint->value1 == $user->gender) {
                    $approved_constraints++;
                }
            }
            if($constraint->survey_constraint_type_id == 1) {
                if(is_null($constraint->value3)) {
                    switch ($constraint->value2) {
                        case '=':
                            if( $constraint->value1 == $user->age() ) {
                                $approved_constraints++;
                            }
                            break;

                        case '<':
                            if( $constraint->value1 >= $user->age() ) {
                                $approved_constraints++;
                            }
                            break;
                        case '>':
                            if( $constraint->value1 <= $user->age() ) {
                                $approved_constraints++;
                            }
                            break;
                    }
                }else {
                    if( $constraint->value1 <= $user->age() && $user->age() <= $constraint->value4 ) {
                        $approved_constraints++;
                    }
                }
            }
        }
        if($approved_constraints == count($survey_constraints)) {
            return true;
        }
        return false;
    }

}
