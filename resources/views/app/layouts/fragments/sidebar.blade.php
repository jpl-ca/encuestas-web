        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Encuestas</a>
            </div>
            <!-- /.navbar-header -->

            @include('app.layouts.fragments.topbar')

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {{--
                        <li>
                            <a href="{{ route('dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        --}}
                        <li>
                            <a href="{{ route('app.surveys.index') }}"><i class="fa fa-check fa-fw"></i> Mis encuestas</a>
                        </li>
                        {{--*/
                            $manual = Encuestas\Libraries\UserManual::getManual();
                        /*--}}
                        <li class="">
                            <a href="javascript:void(0)"><i class="fa fa-files-o fa-fw"></i> Manual de usuario<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @foreach($manual as $indication)
                                <li>
                                    <a class="" href="{{ url('manual-de-usuario').'#'.$indication['id'] }}">{{ $indication['title'] }}</a>
                                </li>
                                @endforeach
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>