<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V20CreateAnswerAlternativesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_alternatives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alternative_type_id')->unsigned();
            $table->string('description')->nullable();
            $table->integer('question_id')->unsigned();
            $table->tinyInteger('min_value')->nullable();
            $table->tinyInteger('max_value')->nullable();
            $table->string('min_tag')->nullable();
            $table->string('max_tag')->nullable();
            $table->string('img_url')->nullable();
            $table->timestamps();

            $table->foreign('alternative_type_id')->references('id')->on('alternative_types');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answer_alternatives');
    }
}
