<?php

namespace Encuestas\Libraries;

class Pusher
{
	public $message = 'This is a pusher message';
	public $title = 'This is a title';
	public $subtitle = 'This is a subtitle.';
	public $tickerText = 'Ticker text.';
	public $vibrate = 1;
	public $sound = 1;
	public $largeIcon = 'large_icon';
	public $smallIcon = 'small_icon';

	protected $registration_ids = array();

	private $api_access_key;

	public function __construct() {
		$this->api_access_key = config('pusher.api_access_key', '');
	}

	public function addRecipent($registration_id) {
		if(is_array($registration_id)) {
			$this->registration_ids = array_merge($this->registration_ids, $registration_id);
		}else {
			array_push($this->registration_ids, $registration_id);
		}
	}

	public function send() {
		$deb = array();
		
		$msg = array(
			'message' => $this->message,
			'title'	=> $this->title,
			'subtitle'	=> $this->subtitle,
			'tickerText'	=> $this->tickerText,
			'vibrate'	=> $this->vibrate,
			'sound'	=> $this->sound,
			'largeIcon'	=> $this->largeIcon,
			'smallIcon'	=> $this->smallIcon
		);

		$fields = array(
			'registration_ids' => $this->registration_ids,
			'data'	=> $msg
		);

		$headers = array(
			'Authorization: key=' . $this->api_access_key,
			'Content-Type: application/json'
		);

		$curl = curl_init();
		curl_setopt( $curl,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $curl,CURLOPT_POST, true );
		curl_setopt( $curl,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $curl,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($curl );
		curl_close( $curl );
		//$result = json_decode($result);
		$deb['msg'] = $msg;
		$deb['fields'] = $fields;
		$deb['headers'] = $headers;
		$deb['result'] = $result;
		return $deb;
	}
}