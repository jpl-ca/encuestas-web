<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    protected $pagename = '';
    protected $pagetitle = '';
    protected $subtitle = '';

    protected function view( $view, array $args = array() ) {
    	$this->handleNames();
    	$_args = ['pagename' => $this->pagename, 'pagetitle' => $this->pagetitle, 'subtitle' => $this->subtitle];
    	$_args = array_merge($_args, $args);
    	return view($view, $_args);
    }

    private function handleNames() {
    	$this->pagename = trim($this->pagename);
    	$this->pagetitle = trim($this->pagetitle);
    	$this->subtitle = trim($this->subtitle);
        
    	$this->pagename = empty( $this->pagename ) ? $this->pagetitle : $this->pagename;
    }
}
