<?php

namespace Encuestas\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use Encuestas\Models\Survey;
use Encuestas\Models\User;

use \Encuestas\Libraries\Pusher;

class ActivateSurveys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:activate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate pending surveys.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->activateSurveys();
    }

    private function activateSurveys() {

        $end_of_day = Carbon::now()->endOfDay()->toDateTimestring();

        $survey_to_be_updated = Survey::where('survey_state_id', 1)
            ->where('start_date', '<', $end_of_day)
            ->get()->lists('id')->toArray();

        $updated_count = Survey::whereIn('id', $survey_to_be_updated)
            ->update(['survey_state_id' => 2]);

        $updated_surveys = Survey::whereIn('id', $survey_to_be_updated)
            ->get();

        $users_with_gcm = User::whereNotNull('gcm_id')->get();
        
        \Log::info( 'TAREA: Activando Encuestas. Ejecutada a las '.Carbon::now()->toDateTimestring().' total de encuestas activadas: '. $updated_count );
        
        if($updated_count > 0) {

            $pusher = new Pusher();
            $pusher->message = 'Hay nuevas encuestas disponibles.';

            foreach ($users_with_gcm as $user) {
                $available_surveys = 0;

                foreach ($updated_surveys as $survey) {
                    if($survey->has_constraints()) {
                        if($survey->has_aproved_constraints($user)) {
                            $available_surveys++;
                        }
                    }else{
                        $available_surveys++;
                    }
                }

                if($available_surveys > 0){
                    $pusher->addRecipent($user->gcm_id);
                }
            }

            $pusher->send();
            
            \Log::info( 'TAREA: Notificaciones enviadas sobre encuestas activadas. Ejecutada a las '.Carbon::now()->toDateTimestring() );
        }
    }
}



