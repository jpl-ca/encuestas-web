<?php

namespace Encuestas\Http\Controllers\API\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Validator;

use Encuestas\Models\User;
use Encuestas\Http\Controllers\ApiController;

use Encuestas\Libraries\Response;
use PaulVL\Helpers\StringHelper;

class AuthController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api-auth', ['only' => ['getLogout']]);
        /*Uncomment on production

        */
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }   

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {

        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'id_doc_number' => 'required|digits:8',
            'password' => 'required',
        ]);
        
        if ($validator->fails()) {
            $validation_errors = StringHelper::concatInOneLine( $validator->errors()->all(), ' ' );
            return Response::unprocessableEntity( $validation_errors );
        }

        if ( Auth::attempt( ['id_doc_number' => $inputs['id_doc_number'], 'password' => $inputs['password'], 'user_type_id' => 3], $request->has('remember') ) ) {
            
            return Response::ok(Auth::user()->toArray());

        }

        return Response::unprocessableEntity( $this->getFailedLoginMessage() );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $credentials = ['user_type_id' => 3]; // solo pueden iniciar sesion los usuarios
        $credentials = array_merge($credentials, $request->only($this->loginUsername(), 'password'));
        return $credentials;
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return trans('auth.failed');
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogout(Request $request)
    {        
        if($request->user()) {            
            Auth::logout();            
        }

        return Response::ok();
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCheck(Request $request)
    {
        if($request->user()) {            
            return Response::ok($request->user()->toArray());
        }
        return Response::unauthorized('No existe una sesión de usuario');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        return property_exists($this, 'username') ? $this->username : 'email';
    }
}
