@extends('app.layouts.base')

@section('base-container')

    <div class="row">
        <div class="col-sm-12 margin-bottom-20">
        	<a href="{{ route('app.surveys.index') }}" class="btn btn-danger pull-right">Cancelar</a>
    	</div>
    </div>

    <div class="row">
        <div class="col-sm-12">

			<div class="panel panel-default">
                <div class="panel-heading">
                    Información básica
                </div>
				
				<form role="form" action="{{ route('app.surveys.store') }}" method="POST" id="newsurveyform" enctype="multipart/form-data">
					{!! csrf_field() !!}
	                <div class="panel-body">
	                    <div class="form-group form-group-lg">
	                        <input class="form-control" name="survey_title" id="survey_title" value="Encuesta sin título" placeholder="Título de la encuesta" autocomplete="off" required>
	                    </div>

						<div class="row margin-bottom-35">
							<div class="col-sm-6">

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="start_date" class="col-sm-5 control-label">Fecha de inicio</label>
											<div class="col-sm-7">
									            <div class='input-group date' id='start_date'>
									                <input type='text' class="form-control" name="start_date" required/>
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
											</div>
										</div>

									</div>
								</div>

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="end_date" class="col-sm-5 control-label">Fecha de fin</label>
											<div class="col-sm-7">
									            <div class='input-group date' id='end_date'>
									                <input type='text' class="form-control" name="end_date" />
									                <span class="input-group-addon">
									                    <span class="glyphicon glyphicon-calendar"></span>
									                </span>
									            </div>
											</div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-5 control-label">Cantidad de encuestados</label>
											<div class="col-sm-7">
									            <input type='text' class="form-control" name="max_completed_per_survey" value="0" max="{{ $max_available_surveys }}" required/>
									        </div>
										</div>

									</div>
								</div>												

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<div class="col-sm-4">
												<div class="radio">
													<label>
														<input type="radio" name="image_type" id="image_type" value="url" aria-label="..." checked>
													</label> Image URL
												</div>
									        </div>
											<div class="col-sm-8">
							                    <div class="form-group">
							                        <input class="form-control" name="image_url" id="image_url" value="" placeholder="" autocomplete="off">
							                    </div>
									        </div>
										</div>

									</div>
								</div>												

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<div class="col-sm-4">
												<div class="radio">
													<label>
														<input type="radio" name="image_type" id="image_type" value="file" aria-label="...">
													</label> Image Archivo
												</div>
									        </div>
											<div class="col-sm-8">
							                    <div class="form-group">
							                        <input type="file" class="xfiles" name="image_file" id="image_file"  accept="image/*" autocomplete="off" disabled>
							                    </div>
									        </div>
										</div>

									</div>
								</div>

							</div>
							<div class="col-sm-6">

								<div class="row margin-bottom-15">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-4 control-label">Genero</label>
											<div class="col-sm-8">
									            <select class="form-control" name="gender_constraint">
													<option value="-1">Ambos</option>
													<option value="0">Femenino</option>
													<option value="1">Masculino</option>
												</select>
									        </div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<label for="max_completed_per_survey" class="col-sm-2 control-label">Edad</label>
											<div class="col-sm-10">
												<div class="checkbox">
													<label>
														<input type="checkbox" name="enable_age_constraints" id="enable_age_constraints" value="1" aria-label="...">Activar
													</label>
												</div>

											</div>
										</div>

									</div>
								</div>												

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">
											<div class="col-sm-2">
												<div class="radio">
													<label>
														<input type="radio" name="age_constraint_type" id="age_constraint_type" value="1" aria-label="..." checked>
													</label>
												</div>
									        </div>
											<div class="col-sm-5">
									            <select class="form-control" name="age_comparison" id="age_comparison">
													<option value="=">iguales a</option>
													<option value=">">mayores a</option>
													<option value="<">menores a</option>
												</select>
									        </div>
											<div class="col-sm-5">
									            <input type='text' class="form-control" name="age_comparison_value" id="age_comparison_value" value="18" max="100" required/>
									        </div>
										</div>

									</div>
								</div>

								<div class="row">
									<div class="col-sm-12">

										<div class="form-group">


											<div class="row">

												<div class="col-sm-2">
													<div class="col-sm-2">
														<div class="radio">
															<label>
																<input type="radio" name="age_constraint_type" id="age_constraint_type" value="2" aria-label="...">
															</label>
														</div>
													</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">desde</label>
														<div class="col-sm-8">
												            <input type='text' class="form-control" name="age_min" id="age_min" value="18" max="99" required/>
												        </div>

									        		</div>
									        	</div>

												<div class="col-sm-5">
													<div class="row">

														<label for="max_completed_per_survey" class="col-sm-3 control-label">hasta</label>
														<div class="col-sm-8">
												            <input type='text' class="form-control" name="age_max" id="age_max" value="25" max="100" required/>
												        </div>

									        		</div>
									        	</div>

									        </div>

										</div>

									</div>
								</div>

							</div>
						</div>

						<h4>Lista de Preguntas 
						<div class="btn-group dropup">
							<button type="button" class="btn btn-default btn-xs add_simple_question_button">Agregar elemento</button>
							<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu">
								<li><a class="add_simple_question_button" href="javascript:void(0)">Opción única</a></li>
								<li><a class="add_multiple_question_button" href="javascript:void(0)">Selección múltiple</a></li>
								{{--
								<li><a href="javascript:void(0)">Rango</a></li>
								--}}
							</div>
						</div></h4>

						<ul class="list-group">

							<div class="questions_wrapper">
							</div>

						</ul>

	                </div>

	                <div class="panel-footer">
						<div class="btn-group dropup">
							<button type="button" class="btn btn-default add_simple_question_button">Agregar elemento</button>
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu">
								<li><a class="add_simple_question_button" href="javascript:void(0)">Opción única</a></li>
								<li><a class="add_multiple_question_button" href="javascript:void(0)">Selección múltiple</a></li>
								{{--
								<li><a href="javascript:void(0)">Rango</a></li>
								--}}
							</div>
						</div>
						<button type="submit" class="btn btn-success pull-right" id="submit">Finalizar</button>
	                </div>

	            </form>
            </div>

        </div>
    </div>

@stop

@section('js')
    <script src="{{ asset('js/new-survey.js')}}"></script>
@stop