<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;

use Encuestas\Models\SurveyConstraintType;

class V20CreateSurveyConstraintTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_constraint_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Model::unguard();

        SurveyConstraintType::create([
            'name' => 'edad'
            ]);

        SurveyConstraintType::create([
            'name' => 'genero'
            ]);

        SurveyConstraintType::create([
            'name' => 'ubicación'
            ]);

        Model::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_constraint_types');
    }
}
