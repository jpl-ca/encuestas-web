<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V23AlterSurveyConstraintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_constraints', function (Blueprint $table) {
            $table->dropColumn('value');
            $table->string('value1')->after('survey_constraint_type_id');
            $table->string('value2')->nullable()->after('value1');
            $table->string('value3')->nullable()->after('value2');
            $table->string('value4')->nullable()->after('value3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_constraints', function (Blueprint $table) {
            $table->dropColumn('value1');
            $table->dropColumn('value2');
            $table->dropColumn('value3');
            $table->dropColumn('value4');
            $table->string('value')->after('survey_constraint_type_id');
        });
    }
}
