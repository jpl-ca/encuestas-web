<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\Controller;

class AppController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*Uncomment on production

        */
    }

    public function getDashboard()
    {
        return redirect()->route('app.surveys.index');
        $this->pagename = 'Dashboard';
        $this->pagetitle = auth()->user()->gender == 0 ? 'Bienvenida' : 'Bienvenido';
        $this->subtitle = auth()->user()->first_name;

        return $this->view('app.layouts.base');
    }
}
