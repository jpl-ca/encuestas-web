@extends('app.layouts.base')

@section('base-container')

    <div class="row">
        <div class="col-sm-12 margin-bottom-20">
        	<a href="{{ route('app.surveys.create') }}" class="btn btn-info pull-right">Crear nueva encuesta</a>
    	</div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        	
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Título</th>
                            <th>Desde</th>
                            <th>Hasta</th>
                            <th>Resultado</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{--*/
                            $total = 0;
                        /*--}}
                    	@foreach($surveys as $survey)
                        {{--*/
                            $total += $survey->current_completed_surveys;
                        /*--}}
                        <tr {{ Encuestas\Libraries\Table::getRowState($survey->survey_state_id) }}>
                            <td>{{ $survey->id }}</td>
                            <td>{{ $survey->title }}</td>
                            <td>{{ $survey->asDate('start_date') }}</td>
                            <td>{{ (is_null($survey->end_date)) ? 'hasta cumplir resultado' : $survey->asDate('end_date') }}</td>
                            <td>{{ $survey->current_completed_surveys.' / '.$survey->max_completed_per_survey }}</td>
                            <td>{{ $survey->survey_state->name }}</td>
                            <td>
                                @if($survey->survey_state_id == 1)
                                <a class="btn btn-xs btn-info" href="{{ route('app.surveys.edit', $survey->id) }}">editar</a>
                                @endif
                                @if( in_array($survey->survey_state_id, [2,3]) )
                                <a class="btn btn-xs btn-default" href="{{ route('app.surveys.results', $survey->id) }}">ver resultados</a>
                                @endif
                                @if($survey->survey_state_id == 2)
                                <a class="btn btn-xs btn-warning" href="{{ route('app.surveys.finish', $survey->id) }}">finalizar</a>
                                <a class="btn btn-xs btn-black" href="{{ route('app.surveys.deactivate', $survey->id) }}">desactivar</a>
                                @endif
                                @if($survey->survey_state_id == 4)
                                <a class="btn btn-xs btn-success" href="{{ route('app.surveys.activate', $survey->id) }}">activar</a>
                                @endif

                                @if($survey->current_completed_surveys == 0 && in_array($survey->survey_state_id, [1,4]))
                                <a class="btn btn-xs btn-danger" href="{{ route('app.surveys.delete', $survey->id) }}">eliminar</a>
                                @endif

                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>Total:</strong></td>
                            <td>{{ $total }}</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
				
			{!! $surveys->render() !!}

        </div>
    </div>

@stop