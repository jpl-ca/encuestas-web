<?php

namespace Encuestas\Http\Controllers;

use Illuminate\Http\Request;

use Encuestas\Http\Requests;
use Encuestas\Http\Controllers\Controller;

use Encuestas\Models\Survey;
use Encuestas\Models\SurveyConstraint;
use Encuestas\Models\Question;
use Encuestas\Models\AnswerAlternative;

use Encuestas\Libraries\Imager;

use PaulVL\Helpers\StringHelper;

use Carbon\Carbon;

class SurveyController extends Controller
{

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        /*Uncomment on production

        */
    }

    public function getIndex()
    {
        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = '';

        $surveys = auth()->user()->surveys()->paginate(25);

        return $this->view('app.surveys.index', compact('surveys'));
    }

    public function getCrear()
    {
        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'crear nueva encuesta';


        $max_available_surveys = auth()->user()->max_available_surveys();

        return $this->view('app.surveys.create', compact('max_available_surveys'));
    }

    public function postGuardar(Request $request)
    {
    	$inputs = $request->all();

    	$_questions = $request->has('question') ? $inputs['question'] : array();
        $_alternatives_descriptions = $request->has('alternative_description') ? $inputs['alternative_description'] : array();
        $_alternatives_types = $request->has('alternative_type') ? $inputs['alternative_type'] : array();
    	$_alternatives_images = $inputs['alternative_image'];
    	$_helptexts = $request->has('helptext') ? $inputs['helptext'] : array();
    	$_mandatories = $request->has('mandatory') ? $inputs['mandatory'] : array();
    	$_question_types = $request->has('question_type') ? $inputs['question_type'] : array();

        $survey_title = $inputs['survey_title'];

        $img_url = null;

        if($request->has('image_url') || $request->hasFile('image_file')){
            $image_name = null;
            if($inputs['image_type'] == 'url' && $request->has('image_url')){
                $image = $inputs['image_url'];
                $image_name = Imager::save($image);
            }
            if($inputs['image_type'] == 'file' && $request->hasFile('image_file')){
                $image = $request->file('image_file');
                $image_name = Imager::save($image);
            }
            if($image_name) {
                $img_url = asset('images/upload/'.$image_name);
            }
        }

    	$max_completed_per_survey = $inputs['max_completed_per_survey'];

    	$start_date = $inputs['start_date'];

    	$end_date = StringHelper::empty_to_null( $inputs['end_date'] );

        //constraints

        $_gender_constraint = $inputs['gender_constraint'];

        $_has_age_constraints = $request->has('enable_age_constraints') ? true : false;

        if($_has_age_constraints) {

            $_age_constraint_type = $inputs['age_constraint_type'];

            $_age_comparison = $inputs['age_comparison'];

            $_age_comparison_value = $inputs['age_comparison_value'];

            $_age_min = $inputs['age_min'];

            $_age_max = $inputs['age_max'];

        }

        ///**-----*-*-*-*-

    	$questions = array();

    	foreach( $_questions as $question_key => $question_value ) {

    		if(isset( $_question_types[$question_key]) ) {
    			$questions[$question_key]['type'] = $_question_types[$question_key];
    		}

    		$questions[$question_key]['title'] = StringHelper::empty_to_null($question_value);

    		if(isset( $_helptexts[$question_key]) ) {
    			$questions[$question_key]['help_text'] = StringHelper::empty_to_null($_helptexts[$question_key]);
    		}

			$questions[$question_key]['mandatory'] = isset( $_mandatories[$question_key] ) ? true : false ;

            if(isset( $_alternatives_descriptions[$question_key]) ) {
                $questions[$question_key]['alternatives'] = array();
                foreach ($_alternatives_descriptions[$question_key] as $alternative_key => $alternative_value) {
                    $alternative_data['alternative_type_id'] = $_alternatives_types[$question_key][$alternative_key];
                    $alternative_data['description'] = StringHelper::empty_to_null($alternative_value);
                    $alternative_data['img_url'] = null;

                    if(!is_null($_alternatives_images[$question_key][$alternative_key])) {
                        $alternative_image_name = false;
                        $filename = 'alternative_image.'.$question_key.'.'.$alternative_key;
                        $image = $request->file($filename);
                        $alternative_image_name = Imager::saveAlternative($image);
                        if($alternative_image_name) {
                            $alternative_data['img_url'] = asset('images/upload/'.$alternative_image_name);
                        }
                    }

                    array_push( $questions[$question_key]['alternatives'], $alternative_data );
                }
            } else {
                $questions[$question_key]['alternatives'] = array();
            }

    	}

        $survey = new Survey;

        $survey->title = $survey_title;
    	$survey->start_date = $start_date;
    	$survey->end_date = $end_date;
        $survey->reward_points = 1;
        $survey->max_completed_per_survey = $max_completed_per_survey;
        $survey->survey_state_id = 1;
        $survey->img_url = $img_url;

        auth()->user()->surveys()->save($survey);

        if($_gender_constraint > -1) {
            $gender_constraint = new SurveyConstraint();
            $gender_constraint->survey_id = $survey->id;
            $gender_constraint->survey_constraint_type_id = 2;
            $gender_constraint->value1 = $_gender_constraint;
            $gender_constraint->save();
        }

        if($_has_age_constraints) {
            if($_age_constraint_type == 1) {
                $age_constraint_type_1 = new SurveyConstraint();
                $age_constraint_type_1->survey_id = $survey->id;
                $age_constraint_type_1->survey_constraint_type_id = 1;
                $age_constraint_type_1->value1 = $_age_comparison_value;
                $age_constraint_type_1->value2 = $_age_comparison;
                $age_constraint_type_1->save();

            }else {
                $age_constraint_type_2 = new SurveyConstraint();
                $age_constraint_type_2->survey_id = $survey->id;
                $age_constraint_type_2->survey_constraint_type_id = 1;
                $age_constraint_type_2->value1 = $_age_min;
                $age_constraint_type_2->value2 = '<=';
                $age_constraint_type_2->value3 = '<=';
                $age_constraint_type_2->value4 = $_age_max;
                $age_constraint_type_2->save();
            }
        }

        foreach($questions as $q) {
            $question = new Question;
            $question->title = $q['title'];
            $question->question_type_id = $q['type'];
            $question->help_text = $q['help_text'];
            $question->mandatory = $q['mandatory'];

            $survey->questions()->save($question);
            
            foreach ($q['alternatives'] as $a) {
                $alternative = new AnswerAlternative;
                $alternative->alternative_type_id = $a['alternative_type_id'];
                $alternative->description = $a['description'];
                $alternative->img_url = $a['img_url'];

                $question->answer_alternatives()->save($alternative);
            }

        }

        return redirect()->action('SurveyController@getIndex')->withSuccess('Encuesta creada satisfactoriamente. Si su encuesta ha sido programada para el día de hoy, se activará automaticamente en 1 hora.');
        //return dd($inputs);
    }

    public function getVerResultados($id)
    {
        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->belongs_to_user_id != auth()->user()->id) return redirect()->back();

        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'resultados de la encuesta #'.$survey->id;

        $results = array();

        foreach ($survey->questions as $question) {
            $results[$question->id] = ['question' => $question->title];
            $results[$question->id]['type'] = $question->question_type_id;
            $alternatives_answers = array();
            $total_answers = 0;
            foreach ($question->answer_alternatives as $answer_alternative) {
                array_push($alternatives_answers, [
                    'name' => $answer_alternative->description,
                    'y' => count($answer_alternative->answers)
                ]);
                $total_answers += count($answer_alternative->answers);
            }
            $results[$question->id]['alternatives'] = $alternatives_answers;
            $results[$question->id]['total_answers'] = $total_answers;
        }

        $chart = new \Encuestas\Libraries\ResultChart($results);

        return $this->view('app.surveys.results', compact('survey', 'results', 'chart'));
    }

    public function getFinalizar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 2) {
            $survey->survey_state_id = 3;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido finalizada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Activa" para poder ser finalizada.');
        }
    }

    public function getDesactivar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 2) {
            $survey->survey_state_id = 4;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido desactivada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Activa" para poder ser desactivada.');
        }
    }

    public function getActivar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->survey_state_id == 4) {
            $survey->survey_state_id = 1;
            $survey->save();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido activada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta tiene que estar "Inactiva" para poder ser activada.');
        }
    }

    public function getEliminar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        if($survey->current_completed_surveys == 0) {
            $survey->delete();
            return redirect()->route('app.surveys.index')->withSuccess('La encuesta ha sido eliminada correctamente.');
        }else {
            return redirect()->route('app.surveys.index')->withWarning('La encuesta no tiene que haber sido respondida para poder ser eliminada.');
        }
    }

    public function getEditar($id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();


        $this->pagename = 'Mis Encuestas';
        $this->pagetitle = 'Mis Encuestas';
        $this->subtitle = 'editando la encuesta #'.$survey->id;


        $max_available_surveys = auth()->user()->max_available_surveys();

        return $this->view('app.surveys.edit', compact('max_available_surveys', 'survey'));
    }

    public function postActualizar(Request $request, $id) {

        if(auth()->user()->user_type_id != 1) {
            $survey = Survey::where('id',$id)->where('belongs_to_user_id', auth()->user()->id)->get();
        }else {
            $survey = Survey::where('id',$id)->get();
        }

        if($survey->isEmpty()) {
            return redirect()->route('app.surveys.index');
        }

        $survey = $survey->first();

        
        $inputs = $request->all();

        $survey_title = $inputs['survey_title'];

        $max_completed_per_survey = $inputs['max_completed_per_survey'];

        if($survey->current_completed_surveys >= $max_completed_per_survey) {
            return redirect()->back()->withWarning('La cantidad de encuestados debe de ser mayor a la cantidad de respuestas ya obtenidas: '.$survey->current_completed_surveys.' .');
        }

        $start_date = $inputs['start_date'];

        $end_date = StringHelper::empty_to_null( $inputs['end_date'] );

        //constraints

        $_gender_constraint = $inputs['gender_constraint'];

        $_has_age_constraints = $request->has('enable_age_constraints') ? true : false;

        if($_has_age_constraints) {

            $_age_constraint_type = $inputs['age_constraint_type'];

            $_age_comparison = $inputs['age_comparison'];

            $_age_comparison_value = $inputs['age_comparison_value'];

            $_age_min = $inputs['age_min'];

            $_age_max = $inputs['age_max'];

        }

        $survey->survey_constraints()->delete();

        if($_gender_constraint > -1) {
            $gender_constraint = new SurveyConstraint();
            $gender_constraint->survey_id = $survey->id;
            $gender_constraint->survey_constraint_type_id = 2;
            $gender_constraint->value1 = $_gender_constraint;
            $gender_constraint->save();
        }

        if($_has_age_constraints) {
            if($_age_constraint_type == 1) {
                $age_constraint_type_1 = new SurveyConstraint();
                $age_constraint_type_1->survey_id = $survey->id;
                $age_constraint_type_1->survey_constraint_type_id = 1;
                $age_constraint_type_1->value1 = $_age_comparison_value;
                $age_constraint_type_1->value2 = $_age_comparison;
                $age_constraint_type_1->save();

            }else {
                $age_constraint_type_2 = new SurveyConstraint();
                $age_constraint_type_2->survey_id = $survey->id;
                $age_constraint_type_2->survey_constraint_type_id = 1;
                $age_constraint_type_2->value1 = $_age_min;
                $age_constraint_type_2->value2 = '<=';
                $age_constraint_type_2->value3 = '<=';
                $age_constraint_type_2->value4 = $_age_max;
                $age_constraint_type_2->save();
            }
        }

        $survey->title = $survey_title;
        $survey->start_date = $start_date;
        $survey->end_date = $end_date;
        $survey->max_completed_per_survey = $max_completed_per_survey;

        $survey->save();

        return redirect()->action('SurveyController@getIndex')->withSuccess('Encuesta actualizada satisfactoriamente. Si su encuesta ha sido programada para el día de hoy, se activará automaticamente en 1 hora.');
    }
}
